package com.sean.sandbox.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sean.sandbox.R
import kotlinx.android.synthetic.main.fragment_three.go_to_4_btn
import kotlinx.android.synthetic.main.fragment_three.go_to_start_from_3_btn
import kotlinx.android.synthetic.main.fragment_three.num_arg_received

class ThreeFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_three, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: ThreeFragmentArgs by navArgs()
        val numReceived: Int = args.num
        num_arg_received.text = numReceived.toString()

        go_to_start_from_3_btn.setOnClickListener { findNavController().navigate(ThreeFragmentDirections.goToStartFrom3()) }
        go_to_4_btn.setOnClickListener { findNavController().navigate(ThreeFragmentDirections.goTo4()) }
    }
}