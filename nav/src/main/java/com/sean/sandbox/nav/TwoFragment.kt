package com.sean.sandbox.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sean.sandbox.R
import kotlinx.android.synthetic.main.fragment_two.go_to_3_btn
import kotlinx.android.synthetic.main.fragment_two.num_arg

class TwoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        go_to_3_btn.setOnClickListener { goTo3() }
    }

    private fun goTo3() {
        val numTxt = num_arg.text.toString()
        val num: Int = when(numTxt.isEmpty()) {
            true -> 0
            else -> numTxt.toInt()
        }
        findNavController().navigate(TwoFragmentDirections.goTo3(num))
    }
}