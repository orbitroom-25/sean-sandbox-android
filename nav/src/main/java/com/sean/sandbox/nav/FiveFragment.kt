package com.sean.sandbox.nav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sean.sandbox.R
import kotlinx.android.synthetic.main.fragment_five.go_to_3_from_5_btn
import kotlinx.android.synthetic.main.fragment_five.go_to_start_btn

class FiveFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_five, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        go_to_3_from_5_btn.setOnClickListener { findNavController().navigate(FiveFragmentDirections.goTo3From5()) }
        go_to_start_btn.setOnClickListener { findNavController().navigate(FiveFragmentDirections.goToStartFrom5()) }
    }
}