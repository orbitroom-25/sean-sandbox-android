package com.sean.sandbox.nav

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.sean.sandbox.R
import kotlinx.android.synthetic.main.activity_nav_main.*

class NavMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_main)
        findNavController(R.id.nav_host).let { nav ->
            toolbar.setupWithNavController(nav, AppBarConfiguration(nav.graph))
        }
    }
}